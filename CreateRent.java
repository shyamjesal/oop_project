import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class CreateRent
{
  public static void main(String[] args) throws ClassNotFoundException
  {
    // load the sqlite-JDBC driver using the current class loader
    Class.forName("org.sqlite.JDBC");
    Random rand = new Random();

    Connection connection = null;
    try
    {
      // create a database connection
      connection = DriverManager.getConnection("jdbc:sqlite:hotels.db");
      Statement statement = connection.createStatement();
      //`statement.setQueryTimeout(30);  // set timeout to 30 sec.

      Statement statement2 = connection.createStatement();

      ResultSet rs = statement.executeQuery("select * from hotels");
      while(rs.next())
      {
        // read the result set
        // System.out.println("name = " + rs.getString("property_name"));
        String hotel_id = rs.getString("uniq_id");
        // System.out.println("hotel_id = " + hotel_id);
        // int room_count = rs.getInt("room_count");
        // System.out.println("room count = " + room_count);
        int rent = rand.nextInt(4000)+1500;
        System.out.println("rent = " + rent);
        System.out.println("uniq_id = " + hotel_id);

          try{
            statement2.executeUpdate("update hotels set rent="+rent+" where uniq_id='"+hotel_id+"'");
          }
          catch(Exception e){
            System.out.println("update me error hai "+e);
          }
      }
    }
    catch(SQLException e)
    {
      // if the error message is "out of memory", 
      // it probably means no database file is found
      System.err.println(e.getMessage());
    }
    finally
    {
      try
      {
        if(connection != null)
          connection.close();
      }
      catch(SQLException e)
      {
        // connection close failed.
        System.err.println(e);
      }
    }
  }
}