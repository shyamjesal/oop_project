public class HotelsInCity
{
	String hotel_id;
	// String hotel_name;
	int rooms_available;

	HotelsInCity(String hotel_id, int rooms_available)
	{
		this.rooms_available = rooms_available;
		this.hotel_id = hotel_id;
	}

	void printDetails()
	{
		System.out.println("Hotel id = " + hotel_id);
		System.out.println("Rooms Available = " + rooms_available);
	}
}