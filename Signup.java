/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author middha
 */

import java.awt.*;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;
import java.awt.event.*;  
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.github.lgooddatepicker.components.*;


class Signup implements ActionListener{
    JLabel labuid;
    JTextField uidtf;
    JLabel labpass;
    JPasswordField passtf;
    JLabel labuname;
    JTextField unametf;
    JLabel labemail;
    JTextField emailtf;
    JLabel labaddress;
    JTextArea addresstf;
    JLabel labdob;
    DatePicker dobpicker;    
    JButton b1;
    JButton b2;
    JFrame f;
    JPanel panel;

    Signup(JFrame f) {
        this.f=f;
       // f = new JFrame("SignUp Page");
        panel = new JPanel(new MigLayout("fillx","[]10[] ", " [] [] [] " ));
        //panel.setBounds(400 , 250, 400, 100);
        panel.setBackground(Color.gray);
        panel.setBorder(BorderFactory.createTitledBorder("SignUp"));

        labuname = new JLabel("User Name:");      //user name label
        panel.add(labuname);
        unametf = new JTextField(20);              //username textfield
        panel.add(unametf,"wrap");

        labemail = new JLabel("Email:");      //Email label
        panel.add(labemail);
        emailtf = new JTextField(20);              //Email textfield
        panel.add(emailtf,"wrap");
       // /*
        labdob = new JLabel("Date Of Birth:");
        dobpicker =new DatePicker();      //Date dobpicker object
        panel.add(labdob);
        panel.add(dobpicker,"growx,wrap"); 
         //*/

        labaddress = new JLabel("Address:");      //Address label
        panel.add(labaddress);
        addresstf = new JTextArea(3,20);              //Address textfield
        panel.add(addresstf,"growx,wrap");

        labuid = new JLabel("User ID:");      //user ID label
        panel.add(labuid);
        uidtf = new JTextField(20);              //userid textfield
        panel.add(uidtf,"wrap");

        labpass = new JLabel("Password:");       //password label
        panel.add(labpass);

        passtf = new JPasswordField(20);     //password field
        panel.add(passtf, "wrap");

          
        b1 = new JButton("Register");
        b2 = new JButton("Go Back to Login Page");
        uidtf.addActionListener(this);
        passtf.addActionListener(this);
        emailtf.addActionListener(this);
         //addresstf.addActionListener(this);
        unametf.addActionListener(this);
         //dobpicker.addActionListener(this);

        b1.addActionListener(this);  
        b2.addActionListener(this); 
        // b1.setBounds(100,100,80,30);    
        // b1.setBackground(Color.green);   
        //panel.add(b1);
        panel.add(b1);// "span,grow");
        panel.add(b2,"growx");
        f.add(panel);
        f.pack();
        //f.setSize(1000, 1000);
        //f.setLayout(null);
        f.setVisible(true);
        
    }
     public void actionPerformed(ActionEvent e) {  
        
        //System.out.println("asdsad");
        if(e.getSource() == b1){  
            try{
               // System.out.println("asdsad1321");
                String uid = uidtf.getText();
                if(!Connect.checkUserid(uid))
                {       
                       // System.out.println("in if");
                        User user1 =new User();
                        user1.userName =unametf.getText();
                        user1.email=emailtf.getText();
                        user1.address=addresstf.getText();
                        user1.dob =dobpicker.getDateStringOrEmptyString();
                        user1.userId=uidtf.getText();  
                        user1.password= new String(passtf.getPassword());  
                        Connect.addUser(user1);
                        panel.setVisible(false);
                        new Login(f); 
                        
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "User Name already exists");
                }
            }
            catch(ClassNotFoundException ae){
               JOptionPane.showMessageDialog(null, "Could Not Connect to Server");
            }
            
        }
        if(e.getSource()==b2){  
            panel.setVisible(false);
            new Login(f); 
        }  
        
    }  
}

 
/* panel.add(firstNameLabel);
    panel.add(firstNameTextField);
    panel.add(lastNameLabel,       "gap unrelated");
    panel.add(lastNameTextField,   "wrap");
    panel.add(addressLabel);
    panel.add(addressTextField,    "span, grow");
 */
