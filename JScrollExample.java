import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.*; 
import java.awt.event.*;  
import javax.swing.*;  
import net.miginfocom.swing.MigLayout;

public class JScrollExample {
    JFrame f;
    JPanel scrollPanel;
    JScrollExample(JFrame f)  
        {  
        this.f=f;     
        try{
                f = getHotels(f);
            }
            catch(ClassNotFoundException e){
                System.out.println("class JDBC not found");
            }
            catch(Exception e){
                System.out.println("unknown error"+e);
            }

        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
        f.pack();
        f.setVisible(true);    
        }  

        public static void main(String args[])  
        {  
            new JScrollExample(new JFrame());              
        } 

        public static class MouseAdapterMod extends MouseAdapter {

            public void mouseClicked(MouseEvent e) {
                JButton button = (JButton)e.getSource();
                System.out.println(button.getName()+" buttton clicked");
            }
        }

        public static JPanel createScrollPanelUnit(JPanel parent,String uid,String name,String type){
            MouseAdapter myMA = new MouseAdapterMod();
            JPanel panel = new JPanel(new MigLayout());
            panel.setBackground(Color.gray);
            Label label_name = new Label();
            label_name.setText(name);
            Label label_type = new Label();
            label_type.setText(type);
            JButton book = new JButton("Book");
            book.addMouseListener(myMA);
            book.setName(uid);
            panel.add(label_name,"span,grow");
            panel.add(label_type,"span,grow");
            panel.add(book,"span,grow");
            parent.add(panel,"wrap");
            return parent;
        }

        public static JFrame getHotels(JFrame f) throws ClassNotFoundException
        {
            // load the sqlite-JDBC driver using the current class loader
            Class.forName("org.sqlite.JDBC");
            JPanel parent = new JPanel(new MigLayout());
            parent.setBackground(Color.black);
            Connection connection = null;
            try
            {
              // create a database connection
              connection = DriverManager.getConnection("jdbc:sqlite:hotels.db");
              Statement statement = connection.createStatement();
              statement.setQueryTimeout(30);  // set timeout to 30 sec.
              ResultSet rs = statement.executeQuery("select * from hotels where state='Orissa'");
              while(rs.next())
              {
                // read the result set
                String uid = rs.getString("uniq_id");
                System.out.println(uid);
                String name = rs.getString("property_name");
                System.out.println(name);
                String type = rs.getString("property_type");
                System.out.println(type);
                parent = createScrollPanelUnit(parent,uid,name,type);
                }
            }
            catch(SQLException e)
            {
              // if the error message is "out of memory", 
              // it probably means no database file is found
              System.err.println(e.getMessage());
            }
            finally
            {
              try
              {
                if(connection != null)
                  connection.close();
              }
              catch(SQLException e)
              {
                // connection close failed.
                System.err.println(e);
              }
                JScrollPane scroll = new JScrollPane(parent,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                scroll.setPreferredSize(new Dimension(500,600));
                scroll.getVerticalScrollBar().setUnitIncrement(10);
                f.add(scroll);
                return f;

            }
          }

    }  