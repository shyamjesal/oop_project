import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.*;

public class Connect_Dahiya
{
    public static ArrayList<HotelsInCity> getSearchResults(HotelQuery hq) throws ClassNotFoundException
    {
        Class.forName("org.sqlite.JDBC");

        java.sql.Connection connection = null;
        try
        {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:hotels.db");
            PreparedStatement pstatement = connection.prepareStatement("select count(distinct testbook.room_id), hotels.room_count, testbook.uniq_id, testbook.room_id from testbook, hotels where hotels.uniq_id = testbook.uniq_id and hotels.city = ? and (testbook.stepoch between ? and ? or testbook.edepoch between ? and ? or testbook.stepoch<=? and testbook.edepoch>=?) group by testbook.uniq_id");
            pstatement.setString(1, hq.city);
            pstatement.setLong(2, hq.checkin);
            pstatement.setLong(3, hq.checkout);
            pstatement.setLong(4, hq.checkin);
            pstatement.setLong(5, hq.checkout);
            pstatement.setLong(6, hq.checkin);
            pstatement.setLong(7, hq.checkout);
            ResultSet rs = pstatement.executeQuery();

            String hotel_id;
            int room_count, booked_rooms, available_rooms;
            HotelsInCity hotel;
            ArrayList<HotelsInCity> arr_hotels = new ArrayList<HotelsInCity>();
            while(rs.next())
            {
                room_count = rs.getInt("hotels.room_count");
                booked_rooms = rs.getInt("count(distinct testbook.room_id");
                available_rooms = room_count - booked_rooms;

                hotel_id = rs.getString("testbook.uniq_id");

                hotel = new HotelsInCity(hotel_id, available_rooms);
                arr_hotels.add(hotel);
            }

            PreparedStatement pstatement1 = connection.prepareStatement("select uniq_id,room_count from hotels where city=? except select distinct testbook.uniq_id,room_count from testbook,hotels where testbook.uniq_id=hotels.uniq_id");
            pstatement1.setString(1, hq.city);
            rs = pstatement1.executeQuery();

            while(rs.next())
            {
                available_rooms = rs.getInt("room_count");
                hotel_id = rs.getString("uniq_id");

                hotel = new HotelsInCity(hotel_id, available_rooms);
                arr_hotels.add(hotel);
            }

            return arr_hotels;
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory", 
            // it probably means no database file is found
            System.err.println(e.getMessage());
            return null;
        }    
        // finally
        // {
        //     try
        //     {
        //         if(connection != null)
        //           connection.close();       
        //     }
        //     catch(SQLException e)
        //     {
        //         // connection close failed.
        //         System.err.println(e);
        
        //     }
        // }
    }
}