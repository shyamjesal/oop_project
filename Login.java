/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author middha
 */

import java.awt.*;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;
import java.awt.event.*;  
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.github.lgooddatepicker.components.*;


    class Login implements ActionListener
    {
        JLabel labuid;
        JTextField uidtf;
        JLabel labpass;
        JPasswordField passtf;
        JButton b1;
        JButton b2;
        JPanel panel;
        JFrame f;
        Login(JFrame f) {
            this.f=f;
            panel = new JPanel(new MigLayout("fillx","[]10[] ", " [] [] [] " ));
            //panel.setBounds(4000 , 250, 400, 100);
            panel.setBackground(Color.gray);
            panel.setBorder(BorderFactory.createTitledBorder("Login"));

            labuid = new JLabel("User ID:");      //user id label
            panel.add(labuid);
             uidtf = new JTextField(20);              //userid textfield
            panel.add(uidtf,"wrap");

            labpass = new JLabel("Password:");       //password label
            panel.add(labpass, "gap unrelated");

            passtf = new JPasswordField(20);     //password field
            panel.add(passtf, "wrap");

            b1 = new JButton("Login");
            // b1.setBounds(50,100,80,30);    
            // b1.setBackground(Color.yellow);   
            b2 = new JButton("Register");
             uidtf.addActionListener(this);
             passtf.addActionListener(this);
            b1.addActionListener(this);  
             b2.addActionListener(this);  
            // b2.setBounds(100,100,80,30);    
            // b2.setBackground(Color.green);   
            panel.add(b1);
            panel.add(b2);// "span,grow");
            f.add(panel);
            f.pack();
            //f.setSize(1000, 1000);
            //f.setLayout(null);
            f.setVisible(true);
            
        }
         public void actionPerformed(ActionEvent e) {  
            String uid=uidtf.getText();  
            String pass= new String(passtf.getPassword());  
            
            if(e.getSource()==b1){  
                try{
                    User user1= Connect.getUser(uid,pass);
                    if(!(user1.userId==null))
                    {
                       panel.setVisible(false);
                       new UserPage(f,user1); 

                        //System.out.println(user1.name);
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "Wrong Username or Password");
                    }
                }
                catch(ClassNotFoundException ae){
                    JOptionPane.showMessageDialog(null, "Could not connect to Server");
                }
                
            }else if(e.getSource()==b2){  
                panel.setVisible(false);
                new Signup(f);  
            }  
            /*String result=String.valueOf(c);  
            tf3.setText(result);  */
    }  

}

 
/* panel.add(firstNameLabel);
    panel.add(firstNameTextField);
    panel.add(lastNameLabel,       "gap unrelated");
    panel.add(lastNameTextField,   "wrap");
    panel.add(addressLabel);
    panel.add(addressTextField,    "span, grow");
 */
