import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;
import java.awt.event.*;  
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.github.lgooddatepicker.components.*;


class SearchHotels implements ActionListener
{
	JLabel lab_city, lab_rooms, lab_checkin, lab_checkout, lab_ppl;
    JComboBox<String> text_city;

    SpinnerModel value_rooms = new SpinnerNumberModel(0,0,10,1);
    SpinnerModel value_ppl = new SpinnerNumberModel(0,0,10,1);
    JSpinner spinner_rooms, spinner_ppl;       
    JButton go, cancel;
    DatePicker checkin_date, checkout_date;

    JPanel panel;
    JFrame f;

    SearchHotels(JFrame f)
    {
    	String city_list[] = {"Mandarmani", "Delhi", "Sri Ganganagar", "Ahmedabad", "Agra"};
    	this.f = f;
    	panel = new JPanel(new MigLayout("","[min!]20[]20[]20[]","100[]50[]50[]100[]30"));
    	panel.setBorder(BorderFactory.createTitledBorder("Search Hotels"));
    
    	lab_city = new JLabel("City");
    	panel.add(lab_city);

    	text_city = new JComboBox<>(city_list);

    	panel.add(text_city,"span 3, growx, wrap");

    	lab_checkin = new JLabel("CheckIn");
    	panel.add(lab_checkin);
		checkin_date = new DatePicker();
    	panel.add(checkin_date);

    	lab_checkout = new JLabel("CheckOut");
    	panel.add(lab_checkout);
    	checkout_date = new DatePicker();
    	panel.add(checkout_date, "wrap");

    	lab_rooms = new JLabel("Rooms");
    	panel.add(lab_rooms);

    	spinner_rooms = new JSpinner(value_rooms);   
    	panel.add(spinner_rooms);

    	lab_ppl = new JLabel("Persons");
    	lab_ppl.setFont(new Font("Helvetica Neue", Font.PLAIN, 14));
    	panel.add(lab_ppl);

    	spinner_ppl = new JSpinner(value_ppl);
    	panel.add(spinner_ppl, "wrap");

    	go = new JButton("Go");
    	cancel = new JButton("Cancel");
    	panel.add(go, "span 4, align right");
    	panel.add(cancel, "align right");

    	go.addActionListener(this);
    	cancel.addActionListener(this);

    	f.add(panel);
        f.pack();
        f.setVisible(true);
    }

    public void actionPerformed(ActionEvent ae)
    {  
        if(ae.getSource()==go)
        {  
        	try
        	{
        		System.out.println("Go pressed");
        		String city = text_city.getItemAt(text_city.getSelectedIndex());
				int rooms = (Integer)spinner_rooms.getValue();
				int persons = (Integer)spinner_ppl.getValue();

				if(city == null || checkin_date == null || 
				   checkout_date == null || rooms == 0 || persons == 0)
				{
					throw new FieldLeftBlankException();
				}
                long checkin, checkout;
                Date in = new Date(), out = new Date();
                try
                {   
                    in = new SimpleDateFormat("yyyy-MM-dd").parse(checkin_date.getDateStringOrEmptyString());
                    
                    out = new SimpleDateFormat("yyyy-MM-dd").parse(checkout_date.getDateStringOrEmptyString());
                    
                }
                catch(ParseException e)
                {
                    e.printStackTrace();
                }

                checkin = in.getTime();
                checkout = out.getTime();
				// Calendar checkin_cal = Calendar.getInstance();
				// checkin_cal.set(Calendar.DAY_OF_MONTH, checkin_date.getDayOfMonth());
				// checkin_cal.set(Calendar.MONTH, checkin_date.getMonth());
				// checkin_cal.set(Calendar.YEAR, checkin_date.getYear());
				// long checkin = checkin_cal.getTimeinMillis();

				// Calendar checkout_cal = Calendar.getInstance();
				// checkout_cal.set(Calendar.DAY_OF_MONTH, checkout_date.getDayOfMonth());
				// checkout_cal.set(Calendar.MONTH, checkout_date.getMonth());
				// checkout_cal.set(Calendar.YEAR, checkout_date.getYear());
				// long checkout = checkout_cal.getTimeinMillis();

        		HotelQuery hq = new HotelQuery(city, checkin, checkout, rooms, persons);

                // This is what you need.
        		try
                {
                    ArrayList<HotelsInCity> arr_hotels = Connect_Dahiya.getSearchResults(hq);
                    for(int i=0; i<arr_hotels.size();i++)
                        arr_hotels.get(i).printDetails();
                }
                catch(ClassNotFoundException e){}
        	}
        	catch(FieldLeftBlankException e)
        	{
        		JOptionPane.showMessageDialog(null, "Fields can't be left blank!");
        	}
        }
        if(ae.getSource()==cancel){  
        	System.out.println("Cancel pressed");
            panel.setVisible(false);
            // new Login(f); 
        }   
    }  
}

class FieldLeftBlankException extends Exception
{
	FieldLeftBlankException()
	{
		super();
	}
}
