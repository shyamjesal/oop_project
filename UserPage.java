import java.awt.*;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;
import java.awt.event.*;  
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.github.lgooddatepicker.components.*;
import java.sql.ResultSet;

class UserPage
{
		JLabel labwelcome;
        JLabel labbooking;
        JPasswordField passtf;
        JButton booknew;
        JButton login;
        JPanel parent;
        JPanel panel1;
        JFrame f;
        User user1;

	UserPage(JFrame f,User user1) throws ClassNotFoundException
	{
		 {
            parent= new JPanel(new MigLayout());
            this.f=f;
            this.user1=user1;
            MouseAdapter myMA = new MouseAdapterMod();
            JButton login = new JButton("Back to Login");
            //login.setName("login");
            parent.add(login,"wrap");
            login.addMouseListener(myMA);
            //JLabel labempty0 = new JLabel("");
            //parent.add(labempty0);
            
            
            JLabel labwelcome = new JLabel("Welcome "+user1.userName+"!");   
            JLabel labempty1 = new JLabel("");
            JLabel labyb =new JLabel("Here are Your Bookings");

            panel1 = new JPanel(new MigLayout("fillx","[]10[] ", " [] [] [] " ));
            panel1.add(labwelcome,"growx ,wrap");
            panel1.add(labempty1,"growx ,wrap");
            panel1.add(labyb,"growx");
            JButton booknew = new JButton("Book a New Hotel");
            panel1.add(booknew,"wrap");
            booknew.addMouseListener(myMA);

            
            //panel.setBounds(4000 , 250, 400, 100);
            panel1.setBackground(Color.gray);
  
            parent.add(panel1,"growx,wrap");
            f.add(parent);
            f.pack();
            Booking[] book;
            book=Connect.bookings(user1);
            int i=0;
            while(book[i]!=null)
            {
            	parent=createScrollPanelUnit(parent,book[i]);
            	i++;
            }
            if (i==0)
            {
            	JLabel nobooking = new JLabel("You have no bookings");
            	JPanel panel2 = new JPanel(new MigLayout());
            	panel2.setBackground(Color.gray);
            	parent.add(panel2,"wrap");
            	panel2.add(nobooking,"grow,wrap");

            }
            //System.out.println(book[0].uniqueId);
            //f.setSize(1000, 1000);
            //f.setLayout(null);
            f.setVisible(true);
            f.pack();
            //panel.setBorder(BorderFactory.createTitledBorder("Login"));
		}
	}
	public static class MouseAdapterMod extends MouseAdapter {

            public void mouseClicked(MouseEvent e) {
                JButton button = (JButton)e.getSource();
                System.out.println(button.getName()+" buttton clicked");
            }
        }


public static JPanel createScrollPanelUnit(JPanel parent,Booking book){
            MouseAdapter myMA = new MouseAdapterMod();
            JPanel panel = new JPanel(new MigLayout());
            panel.setBackground(Color.gray);
            Label label_bid = new Label("Booking Id");
            Label label_hotelname = new Label("Hotel Name");
            Label label_sd = new Label("Checkin Date");
            Label label_ed = new Label("Checkout Date");
            Label label_rooms=new Label("Number of Rooms");
            Label label_nop=new Label("Number of People");
            panel.add(label_bid);
            panel.add(label_hotelname);
            panel.add(label_sd);
            panel.add(label_ed);
            panel.add(label_rooms);
            panel.add(label_nop,"wrap");
            

            Label bid = new Label(book.bookingId);
            Label hotelname = new Label(book.hotel);
            Label sd = new Label(book.startDate);
            Label ed = new Label(book.endDate);
            Label rooms=new Label(Integer.toString(book.rooms));
            Label people=new Label(Integer.toString(book.people));
            panel.add(bid);
            panel.add(hotelname);
            panel.add(sd);
            panel.add(ed);
            panel.add(rooms);
            panel.add(people,"wrap");
            parent.add(panel,"wrap");

            /*
            JButton bid = new JButton(book.bookingId);
            book.addMouseListener(myMA);
            book.setName(uid);
            panel.add(label_name,"span,grow");
            panel.add(label_type,"span,grow");
            panel.add(book,"span,grow");
            parent.add(panel,"wrap");*/
            return parent;
        }
}