public class HotelQuery
{
	String city;
	long checkin, checkout;
	int rooms;
	int persons;

	HotelQuery(String city, long checkin, long checkout, int rooms, int persons)
	{
		this.city = city;
		this.checkin = checkin;
		this.checkout = checkout;
		this.rooms = rooms;
		this.persons = persons;
	}
}