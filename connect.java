import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
 class Connect
{
  public static User getUser(String uname,String pass) throws ClassNotFoundException
  {
    // load the sqlite-JDBC driver using the current class loader
    Class.forName("org.sqlite.JDBC");

    java.sql.Connection connection = null;
    boolean found=false;
    try
    {
      // create a database connection
      connection = DriverManager.getConnection("jdbc:sqlite:users.db");
      Statement statement = connection.createStatement();
      statement.setQueryTimeout(30);  // set timeout to 30 sec.

      // statement.executeUpdate("drop table if exists person");
      // statement.executeUpdate("create table person (id integer, name string)");
      // statement.executeUpdate("insert into person values(1, 'leo')");
      // statement.executeUpdate("insert into person values(2, 'yui')");
      PreparedStatement pstatement =connection.prepareStatement("SELECT * from users WHERE  username = ? and password = ?");
      pstatement.setString(1, uname);
      pstatement.setString(2, pass);

      ResultSet rs = pstatement.executeQuery();
      User user1=new User();
      if(rs.next())
      {   
          user1.userName=rs.getString("name");
	      user1.dob=rs.getString("dob");
	      user1.address=rs.getString("addr");
	      user1.email=rs.getString("email");
	      user1.userId=rs.getString("username");
	      user1.password=rs.getString("password");
      }
      return user1;
    }
    catch(SQLException e)
    {
      // if the error message is "out of memory", 
      // it probably means no database file is found
      System.err.println(e.getMessage());
      return null;
    }
    /*
    finally
    {
      try
      {
        if(connection != null)
          connection.close();
        return true;
      }
      catch(SQLException e)
      {
        // connection close failed.
        System.err.println(e);
        return false;
      }
    }*/
  }
  public static boolean checkUserid(String uname) throws ClassNotFoundException
  {
    // load the sqlite-JDBC driver using the current class loader
    Class.forName("org.sqlite.JDBC");

    java.sql.Connection connection = null;
    boolean found=false;
    try
    {
      // create a database connection
      connection = DriverManager.getConnection("jdbc:sqlite:users.db");
      Statement statement = connection.createStatement();
      statement.setQueryTimeout(30);  // set timeout to 30 sec.

      // statement.executeUpdate("drop table if exists person");
      // statement.executeUpdate("create table person (id integer, name string)");
      // statement.executeUpdate("insert into person values(1, 'leo')");
      // statement.executeUpdate("insert into person values(2, 'yui')");
      PreparedStatement pstatement =connection.prepareStatement("SELECT * from users WHERE  username = ?");
      pstatement.setString(1, uname);
      ResultSet rs = pstatement.executeQuery();
      
      if(rs.next())
      {   
          //System.out.println(rs.getString("username"));
			//System.out.println("poiopi");
         
           found= true;
        // read the result set
        //System.out.println("name = " + rs.getString("name"));
        //System.out.println("username = " + rs.getString("username"));
      }
      return found;
    }
    catch(SQLException e)
    {
      // if the error message is "out of memory", 
      // it probably means no database file is found
      System.err.println(e.getMessage());
      return true;
    }
    /*
    finally
    {
      try
      {
        if(connection != null)
          connection.close();
        return true;
      }
      catch(SQLException e)
      {
        // connection close failed.
        System.err.println(e);
        return false;
      }
    }*/
  }
  public static void addUser(User user1) throws ClassNotFoundException
  {
  		Class.forName("org.sqlite.JDBC");

    java.sql.Connection connection = null;
    try
    {
      // create a database connection
      connection = DriverManager.getConnection("jdbc:sqlite:users.db");
      Statement statement = connection.createStatement();
      statement.setQueryTimeout(30);  // set timeout to 30 sec.

      // statement.executeUpdate("drop table if exists person");
      // statement.executeUpdate("create table person (id integer, name string)");
      // statement.executeUpdate("insert into person values(1, 'leo')");
      // statement.executeUpdate("insert into person values(2, 'yui')");
      PreparedStatement pstatement =connection.prepareStatement("insert into users values(?,?,?,?,?,?)");
      System.out.println(user1.userName);
      pstatement.setString(1, user1.userName);
      pstatement.setString(2, user1.dob);
      pstatement.setString(3, user1.address);
      pstatement.setString(4, user1.email);
      pstatement.setString(5, user1.userId);
      pstatement.setString(6, user1.password);
      pstatement.executeUpdate();
    }
    catch(SQLException e)
    {
      // if the error message is "out of memory", 
      // it probably means no database file is found
      System.err.println(e.getMessage());
    }
    
    finally
    {
      try
      {
        if(connection != null)
          connection.close();
        
      }
      catch(SQLException e)
      {
        // connection close failed.
        System.err.println(e);
        
              }
    }

  }
  public static Booking[] bookings(User user1) throws ClassNotFoundException
  {
      Class.forName("org.sqlite.JDBC");

    java.sql.Connection connection = null;
    Booking[] book=new Booking[100];
    try
    {
      // create a database connection
      connection = DriverManager.getConnection("jdbc:sqlite:hotels.db");
      Statement statement = connection.createStatement();
      statement.setQueryTimeout(30);  // set timeout to 30 sec.

      // statement.executeUpdate("drop table if exists person");
      // statement.executeUpdate("create table person (id integer, name string)");
      // statement.executeUpdate("insert into person values(1, 'leo')");
      // statement.executeUpdate("insert into person values(2, 'yui')");
      PreparedStatement pstatement =connection.prepareStatement("select count(*) as roomcount,start_date,uniq_id,room_id,end_date,booking_id,hotelname,No_of_ppl from testbook where username=? group by booking_id ");
      //System.out.println(user1.userName);
      pstatement.setString(1, user1.userId);  
        ResultSet rs=pstatement.executeQuery();

        int i=0;
        while(rs.next())
        {
          System.out.println(rs.getString("start_date"));
          System.out.println(rs.getString("uniq_id"));
          System.out.println(rs.getString("room_id"));
          System.out.println(rs.getString("end_date"));
          System.out.println(rs.getString("booking_id"));
          System.out.println(rs.getString("hotelname"));
          System.out.println(rs.getString("roomcount"));
          book[i]=new Booking();
          book[i].uniqueId=rs.getString("uniq_id");
          book[i].roomId=rs.getString("room_id");
          book[i].startDate=rs.getString("start_date");
          book[i].endDate=rs.getString("end_date");
          book[i].bookingId=rs.getString("booking_id");
          book[i].hotel=rs.getString("hotelname");
          book[i].rooms=Integer.parseInt(rs.getString("roomcount"));
          book[i].people=Integer.parseInt(rs.getString("No_of_ppl"));

          i++;
         }
        connection.close();
        return book;
       
    }
    catch(SQLException e)
    {
      // if the error message is "out of memory", 
      // it probably means no database file is found
      System.err.println(e.getMessage());
      
      return null;

    }
    
    /*finally
    {
      try
      {
        if(connection != null)
          {
            
            System.out.println("not null connection");
          }        
      }
      catch(SQLException e)
      {
        // connection close failed.
        System.err.println(e);
        return null;
      }
      finally{
        return book;
      }
    }*/


  }
}