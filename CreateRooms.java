import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SampleConnection
{
  public static void main(String[] args) throws ClassNotFoundException
  {
    // load the sqlite-JDBC driver using the current class loader
    Class.forName("org.sqlite.JDBC");

    Connection connection = null;
    try
    {
      // create a database connection
      connection = DriverManager.getConnection("jdbc:sqlite:hotels.db");
      Statement statement = connection.createStatement();
      //`statement.setQueryTimeout(30);  // set timeout to 30 sec.

      Statement statement2 = connection.createStatement();

      // statement.executeUpdate("drop table if exists person");
      // statement.executeUpdate("create table person (id integer, name string)");
      // statement.executeUpdate("insert into person values(1, 'leo')");
      // statement.executeUpdate("insert into person values(2, 'yui')");
      ResultSet rs = statement.executeQuery("select * from hotels, rooms");
      while(rs.next())
      {
        // read the result set
        System.out.println("name = " + rs.getString("property_name"));
        String hotel_id = rs.getString("uniq_id");
        System.out.println("hotel_id = " + hotel_id);
        int room_count = rs.getInt("room_count");
        System.out.println("room count = " + room_count);

        for (int i=0;i< room_count;i++ ) {
          try{
            statement2.executeUpdate("insert into rooms (hotel_id) values('"+hotel_id+"')");
          }
          catch(Exception e){
            System.out.println("update me error hai");
          }
        }
      }
    }
    catch(SQLException e)
    {
      // if the error message is "out of memory", 
      // it probably means no database file is found
      System.err.println(e.getMessage());
    }
    finally
    {
      try
      {
        if(connection != null)
          connection.close();
      }
      catch(SQLException e)
      {
        // connection close failed.
        System.err.println(e);
      }
    }
  }
}