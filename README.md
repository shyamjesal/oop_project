# hotel-management
## steps to replicate my progress
* clone this repo
* compile the java file using "javac -cp miglayout.jar PanelExample.java"
* run using "java -cp .:miglayout.jar PanelExample"
* visit http://www.miglayout.com/ for more info on layout manager.

## steps to use database
* compile the java file using "javac -cp sqlite-jdbc-3.23.1.jar SampleConnection.java"
* run using "java -cp .:sqlite-jdbc-3.23.1.jar SampleConnection"
